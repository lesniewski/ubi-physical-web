#!/bin/sh
### BEGIN INIT INFO
# Provides:          physical-web
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     3 4 5
# Default-Stop:      0 1 2 6
# Short-Description: Node.js /home/pi/apps/physical-web/index.js
# Description:       Physical-Web beacon
### END INIT INFO
 
USER="pi"
 
DAEMON="/usr/local/bin/node"
ROOT_DIR="/home/pi/apps/physical-web"
 
SERVER="$ROOT_DIR/index.js"
LOG_FILE="$ROOT_DIR/index.js.log"
 
LOCK_FILE=/var/run/physical-web.pid

 
do_start()
{
        if [ ! -f "$LOCK_FILE" ] ; then
                echo -n $"Starting $SERVER: "
                su "pi" -c "$DAEMON $SERVER >> $LOG_FILE &" && echo success || echo failure
                RETVAL=$?
                echo
                [ $RETVAL -eq 0 ] && touch $LOCK_FILE
        else
                echo "$SERVER is locked."
                RETVAL=1
        fi
}
do_stop()
{
        echo -n $"Stopping $SERVER: "
        pid=`ps -aefw | grep "$DAEMON $SERVER" | grep -v " grep " | awk '{print $2}'`
        kill -9 $pid > /dev/null 2>&1 && echo success || echo failure
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ] && rm -f $LOCK_FILE
}
 
case "$1" in
        start)
                do_start
                ;;
        stop)
                do_stop
                ;;
        restart)
                do_stop
                do_start
                ;;
        *)
                echo "Usage: $0 {start|stop|restart}"
                RETVAL=1
esac
 
exit $RETVAL

