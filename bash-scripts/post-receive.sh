#!/bin/sh

sudo service physical-web stop

DEPLOYDIR=~/apps/physical-web

GIT_WORK_TREE="$DEPLOYDIR" git checkout -f

cd "$DEPLOYDIR"

npm install

find -path '*bleno*Release/hci-ble' -exec sudo setcap cap_net_raw+eip '{}' \;

sudo service physical-web start

echo "Should be up and running"

