var ngrok = require('ngrok');
var beacon = require('uri-beacon');
var http = require('http');
var connect = require('connect');
var serveStatic = require('serve-static');

var HTTP_SERVER_PORT = 8080;
var WWW_ROOT = '/home/pi/apps/physical-web/www'; // Absolute path to a directory, where static files to serve are

var staticFileServer = connect();
staticFileServer.use(serveStatic(WWW_ROOT));
http.createServer(staticFileServer).listen(HTTP_SERVER_PORT, function() {
  console.log('Server running on port ' + HTTP_SERVER_PORT);

  ngrok.connect(HTTP_SERVER_PORT, function(err, url) {
    console.log("Ngrok URL acquired:", url);
    beacon.advertise(url);
    console.log("Physical Web beacon advertising enabled");
  });
});
